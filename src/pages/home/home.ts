import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  rounds = {
    player1: [],
    player2: []
  }

  sum = {
    player1: 0.0,
    player2: 0.0
  }


  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController
  ) {

  }



  addRound(player) {
    let alert = this.alertCtrl.create({
      title: "Subtract/Add Points",
      subTitle: "Enter a number below.",
      inputs: [
        {
          name: "points",
          placeholder: "Points"
        }
      ],
      buttons: [
        /*{
          text: "Cancel",
          role: "cancel"
        },*/
        {
          text: "Subtract",
          handler: data => {
            const points: number = parseInt(data.points);

            if((data.points.length < 1) || isNaN(points)) {
              return false;
            } else {
              this.rounds[player].push(points * (-1));
              this.updateSums(player);
            }
          }
        },
        {
          text: "Add",
          handler: data => {
            const points: number = parseInt(data.points);

            if((data.points.length < 1) || isNaN(points)) {
              return false;
            } else {
              this.rounds[player].push(points);
              this.updateSums(player);
            }
          }
        }
      ]
    });

    alert.present();
  }


  updateSums(player) {
    let sum: number = 0;
    
    this.rounds[player].forEach(round => {
      sum += round;
    });

    this.sum[player] = sum;
  }

  addRoundForEveryone() {
    Object.keys(this.rounds).reverse().forEach(player => {
      this.addRound(player);
    });
  }
}
